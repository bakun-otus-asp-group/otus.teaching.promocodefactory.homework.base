﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public void AddAsync(Employee newEmployee)
        {
            var newData = Data.ToList();
            newData.Add(newEmployee as T);
            Data = newData;
        }

        public void PutByIdAsync(Guid id, Employee employeeFrom)
        {
            var employeeTo = Data.FirstOrDefault(x => x.Id == id) as Employee;
            if (employeeFrom != null)
            {
                if (employeeFrom.Email != null)
                { 
                    employeeTo.Email = employeeFrom.Email;
                }
                
                if (employeeFrom.Roles != null)
                {
                    employeeTo.Roles = employeeFrom.Roles;
                }
                
                if (employeeFrom.FirstName != null)
                {
                    employeeTo.FirstName = employeeFrom.FirstName;
                }
                
                if (employeeFrom.LastName != null)
                {
                    employeeTo.LastName = employeeFrom.LastName;
                }

                if (employeeFrom.AppliedPromocodesCount != null)
                {
                    employeeTo.AppliedPromocodesCount = employeeFrom.AppliedPromocodesCount;
                }
            }
        }

        public void DeleteAsync(Guid id)
        {
            var newData = Data.ToList();
            var n = Data.FirstOrDefault(x => x.Id == id);
            newData.Remove(n);
            Data = newData;
        }
        
    }
}